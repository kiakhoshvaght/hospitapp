package hak.hospitapp.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import hak.hospitapp.MainActivity;
import hak.hospitapp.R;

public class RoomListFragment extends Fragment {

    private static final String TAG = RoomListFragment.class.getName();
    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View roomListFragmentView = inflater.inflate(R.layout.fragment_room_list, container, false);
        ButterKnife.bind(this,roomListFragmentView);

        mainActivity = (MainActivity)getActivity();

        return roomListFragmentView;
    }

    @OnClick({
            R.id.room_1_ll_room_list_fragment
    })
    public void onClick(View view){
        Log.i(TAG,"FUNCTION : onClick");
        switch (view.getId()){
            case R.id.room_1_ll_room_list_fragment:
                mainActivity.startFragment(new RoomFragment());
        }
    }
}
