package hak.hospitapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.github.shchurov.horizontalwheelview.HorizontalWheelView;
import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hak.hospitapp.MainActivity;
import hak.hospitapp.R;
import hak.hospitapp.modelClasses.Event;

public class RoomFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.temperature_wheel_room_fragment)
    protected HorizontalWheelView temperatureWheel;
    @BindView(R.id.temperature_tv_room_fragment)
    protected TextView temperatureTv;
    @BindView(R.id.reading_lights_switch_room_fragment)
    protected Switch readingLightsSwitch;
    @BindView(R.id.main_lights_switch_room_fragment)
    protected Switch mainLightsSwitch;
    @BindView(R.id.curtain_switch_room_fragment)
    protected Switch curtainSwitch;


    private static final String TAG = RoomFragment.class.getName();
    private MainActivity mainActivity;
    int temprature = 24;
    int delta = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View roomFragmentView = inflater.inflate(R.layout.fragment_room, container, false);
        ButterKnife.bind(this, roomFragmentView);

        mainActivity = (MainActivity) getActivity();
        setupListeners();

        return roomFragmentView;
    }

    private void setupListeners() {
        Log.i(TAG, "FUNCTION : setupListeners");
        curtainSwitch.setOnCheckedChangeListener(this);
        mainLightsSwitch.setOnCheckedChangeListener(this);
        readingLightsSwitch.setOnCheckedChangeListener(this);
        temperatureWheel.setEndLock(true);
        temperatureWheel.setListener(new HorizontalWheelView.Listener() {
            @Override
            public void onRotationChanged(double radians) {
                if(temprature+(int)(radians*8/(1.9*Math.PI))>17 && temprature+(int)(radians*8/(1.9*Math.PI))<33 && delta != (int)(radians*8/(1.9*Math.PI))) {
                    Log.i(TAG, "FUNCTION : setupListeners => Wheel listener => temperature changed");
                    delta = (int)(radians*8/(1.9*Math.PI));
                    temperatureTv.setText(temprature + delta + "");
                    mainActivity.getClient().publishData(new MqttMessage(new Gson().toJson(new Event(3, temprature + delta)).getBytes()));
                }
            }

            @Override
            public void onScrollStateChanged(int state) {

            }
        });
    }

    @OnClick({
    })
    public void onClick(View view) {
        Log.i(TAG, "FUNCTION : onClick");
        switch (view.getId()) {

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Log.i(TAG, "FUNCTION : onCheckedChanged");
        switch (compoundButton.getId()) {
            case R.id.main_lights_switch_room_fragment:
                Log.i(TAG, "FUNCTION : onCheckedChanged => Main lights: " + b);
                mainActivity.getClient().publishData(new MqttMessage(new Gson().toJson(new Event(0, b ? 1 : 0)).getBytes()));
                break;
            case R.id.curtain_switch_room_fragment:
                Log.i(TAG, "FUNCTION : onCheckedChanged => Curtains: " + b);
                mainActivity.getClient().publishData(new MqttMessage(new Gson().toJson(new Event(1, b ? 1 : 0)).getBytes()));
                break;
            case R.id.reading_lights_switch_room_fragment:
                Log.i(TAG, "FUNCTION : onCheckedChanged => Reading lights: " + b);
                mainActivity.getClient().publishData(new MqttMessage(new Gson().toJson(new Event(2, b ? 1 : 0)).getBytes()));
                break;
        }
    }
}
