package hak.hospitapp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hak.hospitapp.fragment.RoomFragment;
import hak.hospitapp.fragment.RoomListFragment;
import hak.hospitapp.helper.MqttHelper;
import rx.Subscriber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MqttCallback {

    private static final String TAG = MainActivity.class.getName();
    @BindView(R.id.mqtt_connection_tv_main_activity)
    protected TextView mqttConnectionTv;
    @BindView(R.id.back_btn_main_activity)
    protected ImageView backBtn;

    private MqttHelper client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUiThings();
        setupMqtt();
    }

    private void setupMqtt() {
        Log.i(TAG,"FUNCTION : setupMqtt");
        client = new MqttHelper(this,this);
        client.connect()
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG,"FUNCTION : setupMqtt => onCompleted");
                        changeMqttConnectionTvStatus(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG,"FUNCTION : setupMqtt => onError: " + e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.i(TAG,"FUNCTION : setupMqtt => onNext: " + aBoolean);
                        changeMqttConnectionTvStatus(true);
                    }
                });
    }

    private void changeMqttConnectionTvStatus(boolean isConnected) {
        Log.i(TAG,"FUNCTION : changeMqttConnectionTvStatus");
        if(isConnected){
            mqttConnectionTv.setText(getString(R.string.mqtt_is_connected));
            mqttConnectionTv.setTextColor(getResources().getColor(R.color.Green_Snake));
        } else {
            mqttConnectionTv.setText(R.string.mqtt_is_disconnected);
            mqttConnectionTv.setTextColor(getResources().getColor(R.color.Red));
        }
    }

    private void setupUiThings() {
        Log.i(TAG,"FUNCTION : setupUiThings");
        setContentView(R.layout.activity_main);
        startFragment(new RoomListFragment());
        ButterKnife.bind(this);
        getSupportActionBar().hide();
    }

    public void startFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout_main_activity, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commit();
    }

    @OnClick({
            R.id.back_btn_main_activity
    })
    @Override
    public void onClick(View view) {
        Log.i(TAG, "FUNCTION : onClick");
        switch (view.getId()) {
            case R.id.back_btn_main_activity:
                Log.i(TAG, "FUNCTION : onClick => backBtn");
                if (getSupportFragmentManager().getFragments().get(0) instanceof RoomFragment) {
                    getSupportFragmentManager().popBackStack();
                    break;
                }
                finish();
                break;
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        Log.i(TAG,"FUNCTION : connectionLost: " + cause.toString());
        changeMqttConnectionTvStatus(false);
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public MqttHelper getClient() {
        return client;
    }
}
