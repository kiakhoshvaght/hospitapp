package hak.hospitapp.modelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("event_type")
    @Expose
    private int eventType;
    @SerializedName("event_info")
    @Expose
    private int eventInfo;

    public Event(int eventType, int eventInfo) {
        this.eventType = eventType;
        this.eventInfo = eventInfo;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public int getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(int eventInfo) {
        this.eventInfo = eventInfo;
    }
}